/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dobbelsteen;

import java.util.Scanner;

/**
 *
 * @author Jeffrey. Jeffrey.de.Boer2@hva.nl
 * Script dat een dobbelsteen gooit waarvan de ogen worden
 * bepaald door invoer vanaf de console. De dobbelsteen blijft werpen
 * totdat deze het nummer 6 bereikt en displayed alle resultaten.
 */
public class Dobbelsteen {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //Define scanner.
        int dobbelsteenWorp = 0; //initialiseer dobbelsteen worp.
        
        System.out.print("Voer een ASCII karakter in "); //output Karakter.
        char oog = input.next().charAt(0); //Voert een karakter van char in.
        
    /*
        While statement om de switch onder controle te houden. Zet limiet op 
        kleiner dan 6. Zolang "kleiner" dan 6 true is blijft dobbelsteen werpen
    */
        
    while (dobbelsteenWorp < 6) {
        dobbelsteenWorp = (int)(Math.random() * 6) + 1;
        
        /*
        Switch met cases voor het uitprinten van het gekozen karakter en het 
        resultaat van de dobbelsteen worp. 
        */
        switch (dobbelsteenWorp) {        
               
            case 1:
            System.out.println("Worp:" + "\n" +
                    " " + " " + " " + "\n" +
                    " " + oog + " " + "\n" +
                    " " + " " + " "); break;
        
            case 2: 
            System.out.println("Worp:" + "\n" +
                    oog + " " + " " + "\n" +
                    " " + " " + " " + "\n" +
                    " " + " " + oog); break;
                    
            case 3:
            System.out.println("Worp:" + "\n" +
                    oog + " " + " " + "\n" +
                    " " + oog + " " + "\n" +
                    " "  + " " + oog); break;
                
            case 4:
            System.out.println("Worp:" + "\n" +
                    oog + " " + oog + "\n" +
                    " " + " " + " " +  "\n" +
                    oog + " " + oog); break;
                
            case 5: 
            System.out.println("Worp:" + "\n" +
                    oog + " " + oog + "\n" +
                    " " + oog + " " + "\n" +
                    oog + " " + oog); break;
               
                           
            case 6:
            System.out.println("Worp:" + "\n" +
                    oog + " " + oog + "\n" +
                    oog + " " + oog + "\n" +
                    oog + " "  + oog); break;
                        
                }
            }
        }
}

